package id.co.passionit.sipit;

/**
 * Created by Fary & Rafa on 15/07/2016.
 */
import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Belal on 11/3/2015.
 */
public interface BookAPI {

    /*Retrofit get annotation with our URL
       And our method that will return us the list ob Book
    */
    @GET("/json2/book.json")
    public void getBooks(Callback<List<Book>> response);
}
